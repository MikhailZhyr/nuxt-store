export const state = () => ({
  product:[]
})

export const getters = {
  product:(state) => state.product
}

export const mutations = {
  SET_PRODUCT(state, product){
    state.product = product
  }
}


export const actions = {
   async getProduct({commit}, productId){
    const response = await this.$axios.get(`products/${productId}`)
    commit('SET_product', response.data)
  }
}
