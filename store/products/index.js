export const state = () => ({
  products:[]
})

export const getters = {
  products:(state) => state.products
}

export const mutations = {
  SET_PRODUCTS(state, products){
    state.products = products
  }
}


export const actions = {
   async getProducts({commit}){
    const response = await this.$axios.get('products/')
    commit('SET_PRODUCTS', response.data)
  }
}
